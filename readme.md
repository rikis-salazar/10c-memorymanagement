# Memory management

We review some material from Pic 10B, more specifically the idea is to
understand how, by sticking to sound programming principles, one can avoid
mishandling memory that was allocated on the heap.

## Memory errors that are not leaks

These are very common errors that most beginners make.  Some of them are
particularly hard to spot because often times _there seems to be nothing
wrong_. For example, consider the code

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
double compute_average( vector<int> scores ){
    int accumulated_average;

    for ( size_t i = 0 ; i < scores.size() ; ++i )
        accumulated_average += scores[i];

    return static_cast<double>(accumulated_average)/scores.size();
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Here, the issue is the uninitialized variable `accumulated_average`. However, if
this variable for some reason holds zero (or a negligible quantity if it was
declared as a `double`), the computation of the return value will either be
correct, or will be _close enough_ to being correct. In both of these cases, a
beginner programmer might not realize the consequences this error will have in
the future. In particular, if on a future run the variable `accumulated_average`
is not close to zero, then the computed average will not be accurate. 

On a similar note, consider the following code

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
double compute_average( vector<int> scores ){
    int *accumulated_average;

    for ( size_t i = 0 ; i < scores.size() ; ++i )
        *accumulated_average += scores[i];

    return static_cast<double>(*accumulated_average)/scores.size();
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Here, the situation although similar, is far more dangerous. Depending on the
_garbage_ memory address stored in `accumulated_average`, the program could 

* run without issues (e.g., if the pointer points to a valid memory address 
  AND this memory address holds a zero value), or 

* it could compute an incorrect average (e.g., if the pointer points to a 
  valid memory address AND this memory address holds a non-zero value), or

* crash unexpectedly.

This is one of the errors that are really hard to debug simply because the
behavior of the program seems to be dependent on some or all of the following
factors:

* whether or not _Julio Urías_ is pitching for the **LA Dodgers** Tonight, or
* whether or not you have received a Facebook notification within the past 
  3.14 seconds, or
* whether or not the  compiler is set with the _debug_ flag on.

### "I think you are exaggerating just a bit..."

If you are indeed thinking that this is simply a beginners error and that it is
really easy to spot, consider this slightly more elaborated version that I am
adapting from one of the files recently submitted for grading in one of my
*Intermediate programming* classes:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class SomeClass{
  private: 
    int field1, field2;
    double sum;
    // Other fields

  public:
    SomeClass( int f1, int f2 ) 
      : fiedl1(f1), field2(f2) { }

    double average ( vector<int> v ){
        for ( size_t i = 0 ; i < v.size() ; ++i )
            sum += v[i];       // Oopsie!

        return sum/v.size();
    }

    ...

};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Here, the sound programming practice that my student failed to follow was:

**Make sure that ALL constructors initialize all class fields.**

As you can see, the private field `sum` was never initialized.  As a result,
some times the program computes the right average, while some other times it
doesn't. 

_Bottom line:_ Memory management should be taken seriously, especially if you
will be handling heap memory. 

While you cannot guarantee that you will never write code that does not leak,
you can certainly try to _program by convention_ and follow a series of rules
that can minimize the occurrence of these types of issues. In the following
lines we will briefly introduce three such rules.


## Wait? What is a memory leak, anyway?

Informally, in C++ we can say that memory that is allocated in the heap (e.g.,
with `new`) that is not properly de-allocated (e.g., with `delete`) constitutes
a _leak_. However, this definition is a bit too narrow. In this course we will
abuse terminology, and refer to any heap mismanagement as a leak.  Here are
some examples of memory mismanagement.

## Calling `new` without calling `delete`.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
void some_fun( ){
    int *p = new int[2017];
    // blah, blah, blah

    return;
}
// Oops. We just lost 2017 int's
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Typical example. Memory is ... "forever lost." Well, actually just for the
duration of the program.  The operating system reclaims the leaked memory after
the misbehaving program terminates.


##  `new` and  `delete` mismatched.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
void some_fun2( ){
    int *p = new int[2017];
    // blah, blah, blah

    delete p;
    return;
}
// Oops. We just lost 2016 int's
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

This a somewhat subtle example. A call to `new type[ SIZE ]` should be paired
with a call to `delete[]`.


## `delete` statement not reached

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
void some_fun3( ){
    int *p = new int[2017];
    double a[ REALLY_BIG_NUMBER ];
    // blah, blah, blah

    delete[] p;
    return;
}
// Leak caused by Stack Overflow
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

This is a lot more subtle. If memory on the stack cannot be allocated (`double
a[ ... ]`), an error handling mechanism [known as an _exception_] is _thrown_.
This in turn causes `some_fun_3()` to skip all code between the memory request
and the closing brace `}` after the return statement. At this point, all local
stack objects are _released_ via a call to their corresponding destructors.
However, heap memory is not released because the `delete[] p` call was
_leapfrogged_.


## Losing sight of heap memory

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
void some_fun4( ){
    int *p; 
    p = new int[2017];
    p = new int[7012];   // Ooops

    delete[] p;
    return;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

There is now no way to release the first block of memory that was requested.


## Premature use of `delete`

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class Stack; 

class StackNode{
  private:
    StackNode* node_below;
    ...

  public:
    StackNode() : node_below( nullptr ) { }
    ...

    friend class Stack;
}

class Stack{
  private:
    StackNode* top_of_stack;

  public:
    StackNode() : top_of_stack( nullptr ) { }

    // requests new StackNode and adds to Stack
    void push();  

    // deletes top_of_stack and updates accordingly
    void pop();   // <-- Assume this works as intended

    ...

    ~Stack() {
        StackNode* p = top_of_stack;
        for ( ; p != nullptr ; p = p->node_below )
            delete p;

        top_of_stack = nullptr;
    }

};

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Did you spot the issue? Let us analyze the case when the destructor is called
on a `Stack` object whose internal `top_of_stack` field is not pointing to
`nullptr`. In this case, in the `for` loop, `p` will be assigned a [valid] heap
address.  This address, in turn, will be released by `delete`, leaving `p` with
an invalid heap address. On the very next statement, namely `p = p->node_below`,
there will be an attempt to de-reference this invalid address, very likely
resulting in a crash.


## Returning a local object

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class Complex{
  private: 
      double real, imaginary;
  public:
      Complex( double r=0., double i = 0. ) 
        : real(r), imaginary(r) { }

      double real_part() const {
          double r = real;
          return r;
      } // OK, works as a getter.

      double& real_part() {
          double r = real;
          return r;
      } // Oops! r is a local variable.

      ...

}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

In this case, returning a local object in the `const` version of `get_real()`
is OK because, in principle, the return is _by value_ (i.e., a copy of the
variable is returned instead). On the other hand, the non-const version returns
the soon-to-be-gone variable `r` by reference. Once control is returned to the
calling function, the reference might no longer be available.


## `delete` before `new` 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
void other_fun( ){
    Complex *p;
    // Call to 'new' lost between commits

    delete p;     // Oops!
    return;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Pretty much self-descriptive, isn't it?


## Double deletion

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
void other_fun( ){
    int *p, *q;; 
    p = new int[2017];
    q = p ;

    delete[] p;     // OK
    delete[] q;     // Oops!
    return;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

This is not a leak in the narrow sense described earlier. However, it will
likely make the program crash.  The actual behavior is undefined.  Although
this seems to be quite an artificial example, it does occur in practice when
one forgets to take good care of ALL of the class constructors. As a particular
[and rather cherry-picked] example consider

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class PointerWrap{
  private:
    int* ptr;

  public:
    PointerWrap( int* p = nullptr ) : ptr(p) { }
    ~PointerWrap(){ delete ptr; }

    // Let compiler fill in the rest
    PointerWrap( const PointerWrap& b ) = default;

    // Disallow transfer of ownership
    PointerWrap& operator=( const PointerWrap& b ) = delete;

};

int bad_fun(){
    PointerWrap P( new int );
    PointerWrap Q(P);

    return 0;
}   // Oops. Double deletion.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Did you spot it? Hint: The copy constructor is, as the name suggests, a
constructor. The line `PointerWrap( const PointerWrap& b ) = default;` can be
interpreted as

> "Dear compiler,
> 
> would you please be so kind to make a copy of the object I am passing as a
> parameter, by simply copying the contents of all of the class fields? Thank
> you in advance. 
>
> Cheers." 

With this in mind, it should be clear that the heap address stored in `P.ptr`
is passed to `delete` twice when `bad_fun()` is releasing stack memory occupied
by `P` and `Q`.

By the way, did I already mention that _all constructors should properly
initialize all member fields_?


## Trying to re-invent the wheel

Although, strictly speaking the following example does not really represent a
leak, it does however, represent an instance of heap misuse. Can you explain
why?

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
void other_fun( ){
    vector<int> *v = new vector<int>;
    // blah, blah, blah

    delete v;
    return;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

`std::vector` vector is a class that, while it does handle heap memory, it does
so via an extra layer of complexity that allows the user not to have to call
the `delete` and `new` operators. Whenever a class exhibits this feature you
should just go ahead and use it.


# Memory mismanagement via inheritance

In this case there are two very common ways to lose data: _slicing_, and calls
to the incorrect overload of a member function.

Let us consider first the _slicing_ case:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class Cosa{          // Thing in Spanish.
  private:
    int m_field;

  public:
    Cosa( int n = 0 ) : m_field(n) {} 
};


class CosaDeColor : public Cosa {   
  private:
    std::string color;

  public:
    CosaDeColor( int n = 0 , std::string c = "blue" )
      : Cosa(n), color(c) { }
};


void some_fun(){
    Cosa c = CosaDeColor(1,"red");
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Although there is only one statement in `some_fun()`, there is a lot going on
there. Namely,

1. A temporary `CosaDeColor` object is created. This object stores the values
   `1` and `red`.
2. A local `Cosa` object is created via the copy constructor. However, since
   `Cosa` objects can only hold an `int`, the `string` value *is not preserved*.
3. The destructor `~CosaDeColor()` is called on the temporary object created in
   step 1.

As the name suggests, the second field gets _"sliced away"_, however this is
not a leak because memory is not really mishandled. The `"red"` value is not
lost, it is actually released by the destructor in step 3. As long as this
behavior is intentional there should be no issues. Of course, if this is not
intentional then there is indeed some data loss. But once again, this *IS NOT*
considered a memory leak.

Consider now the following:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class Cosa{          // Thing in Spanish.
    ...
};


class CosaDeColor : public Cosa {   
    ...
};


void more_fun(){
    Cosa *ptr = new CosaDeColor(1,"red");
    delete ptr;    // <-- Leaks!
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

This is a completely different animal. Here there is no slicing and THERE IS
data loss via memory mismanagement.

First notice that after the first statement, 

1. there is a heap `CosaDeColor` object lying around somewhere,
2. no `Cosa` object exists, and
3. the `CosaDeColor` object is being handled by a `Cosa` pointer.

Because the derived object is being handled by a base pointer, the `delete`
statement calls the `Cosa` destructor instead of the `CosaDeColor` one. This in
turn results in the loss of the `string` object lying somewhere in the heap.
Needless to say that this issue can be fixed easily by calling the right
version of the destructor. 


# OK, OK, OK. I get it! Now, how do I avoid leaks?

The short answer is: _Be careful_. The long answer is: _Even though it is
almost impossible to avoid leaks once you start manually fiddling around with
heap memory, you can stick to the the principles below to try to minimize the
ocurrance of a memory mismanagement_.


## Virtual destructors.

Even if you do not plan to inherit from classes you create, it is always a good
idea to use virtual destructors. This way, when the program is running, the
most-derived overload of a member function is called. In the `Cosa` example,
this results in a call to the most derived destructor `~CosaDeColor()`,
followed by a call to the less derived `~Cosa()` constructor. The `string`
field is then released and the program no longer leaks.


## `try` to request resources safely.

If you find yourself writing a class that needs to handle heap memory, place
any call to `new` within a `try-catch` block of code.

For example, consider the `MyVector` class below which attempts to emulate the
behavior of `std::vector<int>`

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}

const int INITIAL_CAPACITY = 10;

class MyVector{
  private:
    int *theData, size, cap;


  public:
    MyVector();   // See definition below

    virtual ~MyVector() {
        delete theData;
    }

    // Rest of the big 4
    MyVector( const MyVector& );
    MyVector& operator=( const MyVector& );
    ...

};

MyVector::MyVector() 
  : size(0), cap(INITIAL_CAPACITY) {
    theData = new int[cap];
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

If within the constructor the call to `new` fails to allocate memory in the
heap, an exception is thrown. This in turn can [as explained earlier] cause the
memory stack to unwind and can potentially produce some leaks. To avoid this
scenario simply add code that catches the offending [or a more general]
exception.

Here is one possible way to do it.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
MyVector::MyVector() 
  : size(0), cap(INITIAL_CAPACITY) {

    try {
        theData = new int[cap];
    }
    catch( std::exception &e ) {
        std::cerr << "An error occured\n";  // optional
        theData = nullptr;                  // REQUIRED!
    }
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

The extra line `theData = nullptr` in `catch` serves as extra protection. If
for some reason after the exception `e` is thrown `theData` stores an invalid
memory address, the destructor will attempt to access it causing another
exception to be thrown. On the other hand, the call `delete nullptr;` is
actually safe as it results in no action taken by `delete`.

One last thing. These `try-catch` blocks should be placed in every member
function of `MyVector` that results in a call to `new`. Examples of such
functions are other constructors like the copy constructor, as well as members
that alter the size of the vector (e.g., `push_back()`, `resize()`, etc.).


## Use _Copy-and-Swap_  when defining `operator=`

In previous classes you might have been told that to code the assignment
operator one needs to follow 4 steps, namely

1. [**REQUIRED**] Check for self-assignment. 
2. Release heap memory in use by the implicit object.
4. _Deep copy_ the data from the explicit object to the explicit object. This
   step includes a request for new heap memory for the implicit object.
3. Return the implicit object.

While this is perfectly fine for educational purposes, it is better to use an
_exception-safe_ approach that is known as the _Copy-and-Swap_ idiom. These are
the steps:

1. [OPTIONAL] Check for self-assignment. 
2. Make a **local** clone of the explicit parameter via an exception-safe copy
   constructor.
3. Swap the contents of the implicit object with the contents of the clone
   created in the previous step.
4. Return the implicit object. The local clone is released by the class
   constructor.

> Note: _Exception-safe_ does not mean that the copy constructor cannot throw
> an exception [_exception-free_].  Instead, it means that, if an exception
> occurs, objects will remain in a valid state after the exception is handled.

Here are a couple of reasons why this latter approach is preferred: 

* It is _generic_. The code looks the same regardless of whether the class works
  like a vector, or a list, or a matrix, or whatever it is that it is supposed
  to look.
* Unlike in the other approach where forgetting to perform a self-assignment
  check could potentially result in a run-time error, in this case the worst
  thing it could happen is that an extra unnecessary copy is made, resulting in
  a potential performance penalty. In most cases this extra copy is actually
  optimized away by the compiler.

  This is the way it would look for our `MyVector` class:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
MyVector& MyVector::operator=( MyVector clone ){
    swap(*this,clone);
    return *this;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

If this seems to be an act of sorcery, it is because the real work is being
delegated to two very important players: the copy constructor, and the `swap`
function.

As explained earlier, the copy constructor should work as expected and should
be exception-free. On the other hand, the `swap` function must be the correct
one. 

As you might be aware, the function `std::swap(T& x, T& y)` is a template
function that does guarantee that the contents of `x` and `y` are exchanged
safely. If for some reason we decided to _``simplify our lives''_ by including
the statement

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
using namespace std;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

in our library file (say `MyVector.h`), then depending on the actual definition
of `std::swap`, our program could crash because of a stack overflow. The reason
for this is that in some architectures `std::swap` calls the copy constructor,
the destructor, and the assignment operator of the class `T`, as a result the
program enters infinite recursion and it exhaust its stack memory.

So, if not `std::swap`, what else? It should be one that exchanges the contents
of the member fields safely.

For example, the following changes to the `MyVector` class would result in a
working definition of the assignment operator.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class MyVector{
  private:
    int *theData, size, cap;

  public:
    ...

    // member swap that does the right thing
    void swap( MyVector& other ){
        std::swap(theData,other.theData);
        std::swap(size,other.size);
        std::swap(cap,other.cap);
    }

};

// non-member swap that overrides std::swap if 
// parameters are of type 'MyVector'
void swap( MyVector& x, MyVector& y ){
    x.swap(y);
    return;
}

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

The non-member `swap` adds extra insurance. Now regardless of the architecture
and regardless of whether or not we are under the scope of `using namespace
std;` or a similar alias, the code in the assignment operator calls the correct
overload of `swap`. Needless to say that there are other ways to make sure we
do not accidentally call the wrong overload of `swap`. Can you think of at
least one way?


### Wrapping things up

To conclude this [rather long] write-up let us double check that, extra copies
aside, the Copy-and-Swap idiom does indeed compare to other approach mentioned
in the previous section.

* The _self-assignment_ check is not used because it is no longer needed and in
  most cases the compiler makes sure no extra copies are made. 
* The _deep-copy_ part is handled by a combination of a working, exception-safe,
  copy constructor, as well as an appropriate `swap` function.
* Both approaches return the implicit object.
* The data previously stored in the implicit object is released by the class
  destructor. This is because after swapping the class fields, the local `clone`
  object is _the owner_ of the _no-longer-needed_ heap memory. When the
  assignment operator finishes execution, `clone` is safely destroyed releasing
  the heap memory previously assigned to the implicit object.

